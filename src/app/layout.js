import './globals.css'
import ReactQueryProvider from '@/providers/ReactQueryProvider'

export const metadata = {
    title: '🔥🔥PPAZL🔥🔥',
    description: 'DEV 빠즐',
}

export default function RootLayout({ children }) {
    return (
        <html lang="ko">
            <body>
                <ReactQueryProvider>{children}</ReactQueryProvider>
            </body>
        </html>
    )
}
