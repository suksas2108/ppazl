'use client'
import { useQuery } from '@tanstack/react-query'
import React from 'react'
import styles from './index.module.css'

const Index = () => {
    const { data } = useQuery({
        queryKey: ['curation'],
    })

    return (
        <div>
            <ul>
                {data.data.map((v, i) => (
                    <li key={v.curationId} className={i === 1 && styles.highlight}>
                        {v.curationTitle}
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default Index
