import React from 'react'
import { dehydrate } from '@tanstack/query-core'
import { HydrationBoundary, QueryClient } from '@tanstack/react-query'
import { getCuration } from '@/utils/api'
import Index from './ui'

export const dynamic = 'force-dynamic'
export const revalidate = 600

const Main = async () => {
    const queryClient = new QueryClient()

    await queryClient.prefetchQuery({
        queryKey: ['curation'],
        queryFn: getCuration,
    })

    return (
        <HydrationBoundary state={dehydrate(queryClient)}>
            <Index />
        </HydrationBoundary>
    )
}

export default Main
