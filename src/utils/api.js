export const getCuration = async () => {
    const res = await fetch('https://dev-api.ppazl.com/v1/explorer/recommend/contents/curation')
    const json = await res.json()
    return json
}
