/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
    // transpilePackages: ['@tanstack/query-core'], // ios 14이하 지원 필요시 주석 해제 @tanstack/query-core 설치필요?
}

export default nextConfig
